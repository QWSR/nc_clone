import socket_handler

import socket

class clientHandler(socket_handler.socketHandler):
        def connection_init(self):
                super().connection_init()

                self.log_stdout("socket is connected to " + str(self.address) + " on port " + str(self.port))
                self.web_socket.connect((self.address, self.port))


        def send_request(self, data):
                bytes_sent = 0
                total_length = len(data)

                while bytes_sent < total_length:
                        try:
                                byte_batch_length = self.web_socket.send(data)
                                bytes_sent = bytes_sent +  byte_batch_length
                        except Exception as error_msg:
                                self.log_stdout(str(total_length) + " bytes scheduled to send; " + str(bytes_sent) + " bytes sent.")
                                # TODO: error message

                self.log_stdout(str(total_length) + " bytes scheduled to send; " + str(bytes_sent) + " bytes sent.")


        def get_response(self):
                data_item = b""

                data_bit = self.web_socket.recv(1)

                while len(data_bit):
                    if data_bit == b'\n':
                        data_item += data_bit
                        break
                    else:
                        data_item += data_bit
                        data_bit = self.web_socket.recv(1)

                self.log_stdout(str(len(data_item)) + " bytes received in response.")
                return data_item


if __name__ == "__main__":
        c = clientHandler("127.0.0.1", 8800)
        c.connection_init()
        c.send_request(b"whoami\n")
        data = c.get_response()
        print(data.decode("utf-8"))
        c.send_request(b"exit 0\n")
        data = c.get_response()
        print(data.decode("utf-8"))


        exit(0)
