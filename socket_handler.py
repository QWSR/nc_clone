import socket
import signal
import sys
import atexit
import datetime
import traceback

class socketHandler:
    def __init__(self,
                 address,
                 port,
                 connection_log=None,
                 error_log=None,
                 socket_family=socket.AF_INET,
                 socket_type=socket.SOCK_STREAM):

        #member variables
        self.address = address
        self.port = port
        self.socket_family = socket_family
        self.socket_type = socket_type
        self.web_socket = None

        if connection_log:
            self.stdout = open(connection_log, "at")
        else:
            self.stdout = sys.stdout

        if error_log:
            self.stderr = open(error_log, "at")
        else:
            self.stderr = sys.stderr
        
        # special signal; close socket and reopen them when resumed
        #SIGTSTP

        #cleanup function
        atexit.register(self.cleanup)

        signal.signal(signal.SIGHUP, self.cleanup)
        signal.signal(signal.SIGINT, self.cleanup)
        signal.signal(signal.SIGTERM, self.cleanup)


    def connection_init(self):
        self.log_stdout("socket is initiated...")
        self.web_socket = socket.socket(self.socket_family, self.socket_type)


    def log_stdout(self, message, newline="\r\n"):
        log_entry = "[" + str(datetime.datetime.utcnow()) + "] " + message + newline
        self.stdout.write(log_entry)


    def log_stderr(self, error_type, error_message, newline="\r\n"):
        log_entry = "[" + str(datetime.datetime.utcnow()) + "]{ " + error_type + "} " + error_message + newline
        self.stderr.write(log_entry)


    def cleanup(self, signum=None, frame=None):
        if self.stdout != sys.stdout:
            close(self.stdout)
            self.stdout = None

        if self.stderr != sys.stderr:
            close(self.stderr)
            self.stderr = None

        if self.web_socket:
            self.web_socket.close()
            self.web_socket = None

        if signum:
            raise KeyboardInterrupt("socketHandler: socket handler connections have been closed.")
