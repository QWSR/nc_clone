import socket_handler

import subprocess
import socket
import sys
import io

class serverHandler(socket_handler.socketHandler):
    def connection_init(self, num_connections=5):
        super().connection_init()

        self.log_stdout("socket is set to listen on " + str(self.address) + " on port " + str(self.port))

        self.web_socket.bind((self.address, self.port))
        self.web_socket.listen(num_connections)


    def listen_for_requests(self):
        while True:
            connection, client_address = self.web_socket.accept()

            self.log_stdout("connection established with " + str(client_address))

            exit_command = False
            line_data = b""

            while not exit_command:
                data_bit = connection.recv(1)

                if data_bit == b'\n':
                    command = line_data.decode("utf-8")
                    command_arguments = command.split()

                    if len(command_arguments)>0 and command_arguments[0] == "exit":
                        exit_command = True
                    
                    self.log_stdout("running from " + str(client_address) + ": " + str(command))

                    remote_process = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
                    
                    response = remote_process.stdout.read()
                    err_response = remote_process.stderr.read()

                    response_length = 0
                    total_response_length = 0

                    if err_response and len(err_response) > 0:
                        total_response_length += len(err_response)
                        err_resp_len = connection.send(err_response)
                        response_length += err_resp_len

                    if response and len(response) > 0:
                        total_response_length += len(response)
                        resp_len = connection.send(response)
                        response_length += resp_len

                    self.log_stdout(str(total_response_length) + " bytes scheduled to send; " + str(response_length) + " bytes sent.")

                    line_data = b""
                else:
                    line_data += data_bit

            self.log_stdout("shutting down " + str(client_address))
            connection.shutdown(socket.SHUT_RDWR)
            connection.close()


if __name__ == "__main__":
	s = serverHandler("127.0.0.1", 8800)
	s.connection_init()
	s.listen_for_requests()
	exit(0)

